# blinky

Repository for make test on CC.

# Setup

Install Ceedling ==> http://www.throwtheswitch.org/ceedling

# Instructions

1. Create a new project

    $ ceedling new blinky

2. Initialize a git repository

    $ cd blinky
    $ git init . && git add . && git commit -m "Initial commit."

3. Create our first module

    $ ceedling module:create[led]

4. Run our first test

    $ ceedling test:all

# Test plan

Now we are ready to beginning with our test plan.

    1. Initialize properly
    2. Turn on
    3. Turn off
    4. Read the states

# Start to coding!